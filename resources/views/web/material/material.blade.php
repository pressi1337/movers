@extends('web.layout.master')

@section('content')

<style>
    .accordion-title {
        background-color:#4680ff;
        color: white !important;
    }
    .the-body{
        background-color: aliceblue;
    }
     
</style>


{{-- page header --}}
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Material</h4>
                    <span>Add Material to choose by clients</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                    <a href="{{ url('admin/dashboard') }}"> <i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ url('material') }}">Material</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
{{-- ./page header --}}

{{-- page body --}}
<div class="page-body">
    <div class="row">
        <div class="row col-sm-4">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h5 class=""><b>Add Material</b></h5>
                    </div>
                    <div class="card-block">
                        <form action="{{ url('material/add') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Material Name <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="add material name that user can select"></i> </label>
                            <div class="col-sm-12">
                                <input type="text"  class="form-control"  name="material" placeholder="Add Client Name"   required>
                            </div>
                        </div>
                        
                        <div class="text-center">
                            <button class="btn btn-mat btn-info ">Add </button>
                        </div>
                        </form>
                        
                        
                    </div> 
                </div>
            </div>
        </div>
        <div class="row col-sm-8">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header ">
                    <h5>Manage Material</h5>
                    <span>Manege your all Material Below</span>
                    </div>
                    <div class="card-block">
                        <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="row">

                                @foreach($material as $cl)
                                <div class="col-md-3">
                                  <div class="card">
                                    <div class=" text-right">
                                      <a href="{{ url('') }}/material/delete/{{ $cl->id }}"><i class="fa fa-trash f-30 text-c-pink"></i></a>
                                    </div>
                                    <p class="text-center">{{ $cl->material }}</p>
                                  </div> 
                                </div>
                                @endforeach
                      
                               
                                
                              
                                
                        </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>
{{-- ./page body --}}



@endsection
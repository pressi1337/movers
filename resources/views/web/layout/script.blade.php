<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>



<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script  type="text/javascript" src="{{ url('') }}/admin/files/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/modernizr/js/css-scrollbars.js"></script>


<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/switchery/js/switchery.min.js"></script>

<script type="text/javascript" src="{{ url('') }}/admin/files/assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/assets/pages/j-pro/js/jquery.j-pro.js"></script>


<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
<script src="{{ url('') }}/admin/files/cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js" type="text/javascript"></script>

<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/bootstrap-maxlength/js/bootstrap-maxlength.js"></script>

<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/i18next/js/i18next.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
{{-- setting --}}
<script type="text/javascript" src="{{ url('') }}/admin/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>

<script type="text/javascript" src="{{ url('') }}/admin/files/bower_components/datedropper/js/datedropper.min.js"></script>

<script src="{{ url('') }}/admin/files/bower_components/datatables.net/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{ url('') }}/admin/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{ url('') }}/admin/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{ url('') }}/admin/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{ url('') }}/admin/files/assets/pages/chart/echarts/js/echarts-all.js" type="text/javascript"></script>
<script src="{{ url('') }}/admin/files/assets/pages/user-profile.js" type="text/javascript"></script>


<script type="text/javascript" src="{{ url('') }}/admin/files/assets/pages/advance-elements/swithces.js"></script>
{{-- <script type="6832da077c7f630111f86fc0-text/javascript" src="{{ url('') }}/admin/files/assets/pages/advance-elements/swithces.js"></script> --}}
<script src="{{ url('') }}/admin/files/assets/js/pcoded.min.js" type="text/javascript"></script>
<script src="{{ url('') }}/admin/files/assets/js/vartical-layout.min.js" type="text/javascript"></script>
<script src="{{ url('') }}/admin/files/assets/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('') }}/admin/files/assets/js/script.js"></script>

<!-- <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
      var options = {
        filebrowserImageBrowseUrl: "{{ url('') }}/laravel-filemanager?type=Images",
        filebrowserImageUploadUrl: "{{ url('') }}/laravel-filemanager/upload?type=Images&_token=",
        filebrowserBrowseUrl: "{{ url('') }}/laravel-filemanager?type=Files",
        filebrowserUploadUrl: "{{ url('') }}/laravel-filemanager/upload?type=Files&_token="
      };
      CKEDITOR.replace( 'summary-ckeditor',options);
</script> -->


<script src="{{ url('') }}/admin/files/ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="6832da077c7f630111f86fc0-|49" defer=""></script>

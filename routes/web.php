<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//notAuth
Route::get('login','Web\AuthController@login');
Route::post('login','Web\AuthController@login');
Route::get('forgotpassword','Web\AuthController@verify_mailToChangeNewpassowrd');


//Auth
Route::group(['middleware' => 'checkuser','namespace'=>'Web'],function(){
	Route::get('dashboard','DashboardController@dashboard');
    Route::get('transport/holder','TransportManagerController@index');
    Route::get('users','UsersController@index');



    //add material
    Route::get('material','MaterialController@material');
    Route::post('material/add','MaterialController@material');
    Route::get('material/delete/{id}','MaterialController@material_delete');

});
	


<!-- navigation -->
			<nav class="pcoded-navbar">
				<div class="pcoded-inner-navbar main-menu">
				  <div class="pcoded-navigatio-lavel">Navigation</div>
					<ul class="pcoded-item pcoded-left-item">
						

						<li class="">
							<a href="{{ url('dashboard') }}">
							<span class="pcoded-micon"><i class="feather icon-home"></i></span>
							<span class="pcoded-mtext">Dashboard</span>
							</a>
						</li>
						<li class="">
							<a href="{{ url('transport/holder') }}">
							<span class="pcoded-micon"><i class="feather icon-home"></i></span>
							<span class="pcoded-mtext">Transport Holders</span>
							</a>
						</li>
						<li class="">
							<a href="{{ url('users') }}">
							<span class="pcoded-micon"><i class="feather icon-home"></i></span>
							<span class="pcoded-mtext">Users</span>
							</a>
						</li>
						<li class="">
							<a href="{{ url('material') }}">
							<span class="pcoded-micon"><i class="feather icon-home"></i></span>
							<span class="pcoded-mtext">Materials</span>
							</a>
						</li>
						<li class="pcoded-hasmenu active pcoded-trigger">
							<a href="javascript:void(0)">
							<span class="pcoded-micon"><i class="feather icon-home"></i></span>
							<span class="pcoded-mtext">Report</span>
							</a>
							<ul class="pcoded-submenu">
								<li class="">
									<a href="index.html">
									<span class="pcoded-mtext">Post</span>
									</a>
								</li>
								<li class="active">
									<a href="dashboard-crm.html">
									<span class="pcoded-mtext">Activity</span>
									</a>
								</li>
							
							</ul>
						</li>
						
					</ul>
				</div>
			</nav>
			<!-- end navigation -->
<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class TransportManagerController extends Controller
{
    //
    public function index(){

    	$this->data['transports']= DB::table('users')->where('otp_verified','yes')->where('istype','1')->get();
    	return view('web.transport.transport',$this->data);
    }
}

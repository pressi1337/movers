<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;

class MaterialController extends Controller
{
    //add material
    public function material(Request $request){

    	if($_POST){

    	    $material = $request->material;
            DB::table('material')->insert([
              'material'=> $material 
            ]);
            return redirect('material')->with('message','added Successfully');

    	}

    	$this->data['material'] = DB::table('material')->get();


    	return view('web.material.material',$this->data);

    }


    //remove the materials
    public function material_delete($id){
    	
         DB::table('material')->where('id',$id)->delete();
        return redirect('material')->with('message','Removed Successfully');
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


/** 
* WebApplication Api
* 
**/

Route::group([
    'prefix'=>'admin',
    'namespace'=>'Api\Admin'
],function(){
    Route::post('login','AuthController@login');
    Route::post('forgotpassword','AuthController@verifyEmailToForgotPassword');
    Route::post('forgotpassword/otp','AuthController@verifyOtpToUpdatePassword');
    Route::post('forgotpassword/otp/updatepassword','AuthController@updateNewPassword');
});




/** 
* Android Application Api
* 
**/

Route::group([
	'prefix'=>'user',
    'namespace'=>'Api',
],function(){
	Route::post('register','AuthController@register');
	Route::post('login','AuthController@login');
	Route::post('verify/otp','AuthController@verify_otp');
	Route::post('forgotpassword/sentotp','AuthController@verifyMobileToForgotPassword');
	Route::post('forgotpassword/verify/otp','AuthController@verifyOtpToUpdatePassword');
	Route::post('forgotpassword/update/newpassword','AuthController@updateNewPassword');

});
@extends('web.layout.master')

@section('content')


<style type="text/css">
	@media only screen and (max-width: 920px) {
		  .table-responsive {
			    display: inline-block;
			    width: 100%;
			    overflow-x: auto; 
			 }
	}

	@media only screen and (max-width: 1500px) {
		  .table-responsive {
			    display: inline-block;
			    width: 100%;
			    overflow-x: auto; 
			 }
	}

	
}
</style>

<div class="page-header">
<div class="front-icon-breadcrumb row align-items-end">
<div class="breadcrumb-header col">
<div class="big-icon">
<i class="icofont icofont-ui-user-group"></i>
</div>
<div class="d-inline-block">
<h5>Manage Users</h5>
<span>Listed below all your  User</span>
</div>
</div>
<div class="col">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">

<li class="breadcrumb-item"><a href="{{ url('dashboard') }}"> <i class="feather icon-users"></i> </a>
</li>
<li class="breadcrumb-item"><a href="{{ url('users') }}">User Manage</a>
</li>
</ul>
</div>
</div>
</div>
</div>


<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<h5>Listed Users</h5>
<span>Listed below to manage Users</span>
</div>
<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable"  class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>Name</th>
<th>Mobile</th>
<th>Created At</th>
<th>status</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php $count = 1; if(isset($users) && !empty($users)){ ?>
@foreach($users as $user)

<tr>
<td>{{ $user->name }}</td>
<td>{{ $user->mobile }}</td>
<td>{{ $user->created_at }}</td>
<td>{{ $user->block }}</td>
<td><a class="btn btn-success" href="#">View</a></td>
</tr>
@endforeach
<?php } ?>
</tbody>

</table>
</div>
</div>
</div>

</div>
</div>
</div>





@endsection

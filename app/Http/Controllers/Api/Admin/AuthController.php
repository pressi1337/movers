<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use JWTAuth;
use Validator;
use Crypt;

class AuthController extends Controller
{
    protected $admin;

	public function __construct()
	{
	   $this->admin = new Admin;
	}



	 /**
     * Verify Email Address to get OTP
     *
     */
    public function verifyEmailToForgotPassword(Request $request){
    	$validator = Validator::make($request->all(),
	    [
	      'email'=>'required|email',
	    ]);

	    if($validator->fails())
	    {
	     return response()->json([
	         "success"=>false,
	         "message"=>$validator->messages()->toArray(),
	     ],400);
	    }

        //check vaild Email
	    $check_email = $this->admin->where("email",$request->email)->count();

	    if($check_email == 0)
	    {
	        return response()->json([
	        'success'=>false,
	        'message'=>'invalid Email address'
	        ],400);

	    }

	    if($check_email){
	    	$generateOtp = rand(11111,99999);
	    	$rembertoken = Crypt::encrypt($generateOtp);
	    	$this->admin->where('email',$request->email)->update(['otp'=>$generateOtp,
	    		'remember_token'=>$rembertoken,
	    ]);
	    	//sent otp to sms api


	    	return response()->json([
	        'success'=>false,
	        'message'=>'OTP sent to your Email address',
	        'rembertoken'=>$rembertoken
	        ],200);

	    }

    }


    /**
     * Verify OTP To update password
     *
     */
    public function verifyOtpToUpdatePassword(Request $request){

    	$validator = Validator::make($request->all(),
	    [
	      'otp'=>'required|integer',
	      'rembertoken'=>'required'
	    ]);

	    if($validator->fails())
	    {
	     return response()->json([
	         "success"=>false,
	         "message"=>$validator->messages()->toArray(),
	     ],400);
	    }

	    $check_vaild_otp = $this->admin->where('otp',$request->otp)->where('remember_token',$request->rembertoken)->count();
	    if($check_vaild_otp){
            
            $rembertoken = Crypt::encrypt(rand(1111,9999));
	    	$this->admin->where('remember_token',$request->rembertoken)->where('otp',$request->otp)->update(['remember_token'=>$rembertoken]);
	    	return response()->json([
	        'success'=>false,
	        'message'=>'otp verified successfully',
	        'rembertoken'=>$rembertoken
	        ],200);

	    }else{
	    	return response()->json([
	        'success'=>false,
	        'message'=>'invalid otp number'
	        ],400);
	    }

    }


    /**
     * Verify the otp and update new password
     *
     */
    public function updateNewPassword(Request $request){
    	$validator = Validator::make($request->all(),
	    [
	      'rembertoken'=>'required',
	      'newpassword'=>'required|string|min:6',
	    ]);

	    if($validator->fails())
	    {
	     return response()->json([
	         "success"=>false,
	         "message"=>$validator->messages()->toArray(),
	     ],400);
	    }

	    //check vaild token
	    $check_vaild_token = $this->admin->where("remember_token",$request->rembertoken)->count();

	    if($check_vaild_token == 0)
	    {
	        return response()->json([
	        'success'=>false,
	        'message'=>'invalid token'
	        ],400);

	    }

	    if($check_vaild_token){
	    	$NewPassword = Hash::make($request->newpassword);
	    	//update Newpassword
	    	$this->admin->where('remember_token',$request->rembertoken)->update(['password'=>$NewPassword]);
	    	return response()->json([
	        'success'=>false,
	        'message'=>'New password update successfully'
	        ],200);
	    }

    }






	 /**
     * Login
     *
     */
     public function login(Request $request){

    	$validator = Validator::make($request->only('email','password'),
	     [
	        'email'=>'required|email',
	        'password'=>'required',
	     ]);

    	 if($validator->fails())
	     {
		     return response()->json([
		         "success"=>false,
		         "message"=>$validator->messages()->toArray(),
		     ],400);
	     }

	     $jwt_token = null;

	     $input = $request->only("email","password");

	     $check_email = $this->admin->where("email",$request->email)->count();

	    if($check_email == 0)
	    {
	        return response()->json([
	        'success'=>false,
	        'message'=>'invalid email address'
	        ],400);

	    }
	     //Authentication
	     if(!$jwt_token = auth('admin')->attempt($input))
	     {
	        return response()->json([
	            'success'=>false,
	            'message'=>'invalid password'
	        ],400);

	      }
	      $admin = auth("admin")->authenticate($jwt_token);

	     return response()->json([
			  'success'=>true,
			  'token'=>$jwt_token,
			 ],200);

    }
}

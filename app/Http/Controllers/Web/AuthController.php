<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use Validator;
use Crypt;
use Session;
use DB;


class AuthController extends Controller
{
    
    protected $admin;

	public function __construct()
	{
	   $this->admin = new Admin;
	}



	/**
     * Login
     *
     */
     public function login(Request $request){

    	if($request->isMethod('post')){
		    	$validator = Validator::make($request->only('email','password'),
			     [
			        'email'=>'required|email',
			        'password'=>'required',
			     ]);

		    	 

			     $input = $request->only("email","password");

			     $check_email = $this->admin->where("email",$request->email)->count();

			    if($check_email == 0)
			    {

			       return redirect('/login')->with('message',' invalid email address ');

			    }
			     //Authentication
			     if(auth('admin')->attempt($input))
			     {
		          //$this->middleware("auth:admin");
			     	Session::put('uname',$request->email);  
			        return redirect('/dashboard')->with('message',' login successfull ');
			     }else{
			     	return redirect('/login')->with('message',' invalid password ');
			     }


			     return redirect('/login')->with('message',' login successfull ');
		    }

		    return view('web.login.login');
    	}



  
        //forgot password section
    	public function verify_mailToChangeNewpassowrd(){
    		return view('web.login.forgotpassword');
    	}




}

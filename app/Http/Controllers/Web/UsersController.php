<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class UsersController extends Controller
{
    //
     public function index(){

    	$this->data['users']= DB::table('users')->where('otp_verified','yes')->where('istype','0')->get();
    	return view('web.users.users',$this->data);
    }
}

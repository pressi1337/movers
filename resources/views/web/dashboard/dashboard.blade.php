@extends('web.layout.master')

@section('content')

<style type="text/css">
	#card_0{
		border-left: 4px solid #20A9AC;
	}
	#card_1{
		border-left: 4px solid #FE5D70;
	}
	#card_2{
		border-left: 4px solid #0AC282;
	}
	#card_3{
		border-left: 4px solid #FE9365;
	}
	/*._reant_border{
		border:1px solid green;
	}*/
	._recent_icon{
		margin: auto;
	}
</style>

<div class="page-header">
 <div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Hi, welcome back!</h4>
<span>Your customer service/help desk monitoring dashboard </span>
</div>
</div>
</div>
<div class="col-lg-4">
<div class="page-header-breadcrumb">
<ul class="breadcrumb-title">
<li class="breadcrumb-item">
<a href="index.html"> <i class="feather icon-home"></i> </a>
</li>
<li class="breadcrumb-item"><a href="{{ url('') }}">Dashboard</a>
</li>

</ul>
</div>
</div>
</div>
</div>



<div class="row">
    <div class="col-xl-3 col-md-6">
        <div class="card " id="card_0">
        <div class="card-block">
        <div class="row align-items-center">
        <div class="col-8">
        <h2 class="text-c-blue f-w-600">7</h2>
        <h6 class="f-w-600 m-b-0">Vehicle User</h6>
         </div>
        <div class="col-4 text-right">
        <!-- <i class="fa fa-eye f-28"></i> -->
        <!--https://www.flaticon.com/svg/static/icons/svg/0/387.svg https://www.flaticon.com/svg/static/icons/svg/664/664468.svg -->
        <img src="https://www.flaticon.com/svg/static/icons/svg/664/664468.svg"  width="60" height="60">

        </div>
        </div>
        </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6">
        <div class="card" id="card_1">
        <div class="card-block">
        <div class="row align-items-center">
        <div class="col-8">
        <h2 class="text-c-pink f-w-600">6</h2>
        <h6 class="f-w-600 m-b-0">User</h6>
        </div>
        <div class="col-4 text-right">
        <!-- <i class="fa fa-bullseye f-28"></i> -->
        <img src="https://www.flaticon.com/svg/static/icons/svg/16/16480.svg" width="60" height="60">
        </div>
        </div>
        </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6">
        <div class="card" id="card_2">
        <div class="card-block">
        <div class="row align-items-center">
        <div class="col-8">
        <h2 class="text-c-green f-w-600">2</h2>
        <h6 class="f-w-600 m-b-0">Task Completed</h6>
         </div>
        <div class="col-4 text-right">
        <!-- <i class="fa fa-mobile f-28"></i> 
          https://www.flaticon.com/svg/static/icons/svg/3078/3078926.svg
        -->
        <img src="https://www.flaticon.com/svg/static/icons/svg/1292/1292983.svg" width="60" height="60">
        </div>
        </div>
        </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card" id="card_3">
        <div class="card-block">
        <div class="row align-items-center">
        <div class="col-8">
        <h2 class="text-c-yellow f-w-600 ">3</h2>
        <h6 class="f-w-600 m-b-0">Total Booking</h6>
        </div>
        <div class="col-4 text-right">
       <!--  <i class="fa fa-desktop f-28"></i> -->
       <img src="https://www.flaticon.com/svg/static/icons/svg/2636/2636428.svg" width="60" height="60">
        </div>
        </div>
        </div>
        </div>
    </div>
</div>
{{-- ./carding block --}}


<div class="page-body">
	<div class="row">

		
		<div class="col-xl-4 col-md-12">
<div class="card feed-card">
<div class="card-header">
<i class="fa fa-bullhorn" style="font-size: 1.2rem;"></i> 
<h5> Recent Task</h5>
</div>
<div class="card-block">
<div class="row m-b-30 _reant_border" >
<div class="col-auto p-r-0  _recent_icon">
<i class="feather icon-bookmark bg-simple-c-yellow feed-icon"></i>
</div>
<div class="col">
<h6 class="text-muted">TrackNumber 100009809</h6>
<h6 class="m-b-5">Prasad <i class="fa fa-arrows-h"></i> shiva transport <span class="text-muted f-right f-13 label label label-warning">Booked</span></h6>
</div>
</div>

<div class="row m-b-30 _reant_border" >
<div class="col-auto p-r-0  _recent_icon">
<i class="feather icon-alert-triangle bg-simple-c-pink feed-icon"></i>
</div>
<div class="col">
<h6 class="text-muted">TrackNumber 100009468</h6>
<h6 class="m-b-5">Santhosh <i class="fa fa-arrows-h"></i> shiva transport <span class="text-muted f-right f-13 label label-danger">Cancelled</span></h6>
</div>
</div>

<div class="row m-b-30 _reant_border" >
<div class="col-auto p-r-0  _recent_icon">
<i class="feather icon-check bg-simple-c-green feed-icon"></i>
</div>
<div class="col">
<h6 class="text-muted">TrackNumber 100006500</h6>
<h6 class="m-b-5">Rahul <i class="fa fa-arrows-h"></i> Kpn transport <span class="text-muted f-right f-13 label label-success">Completed</span></h6>
</div>
</div>

<div class="text-center">
<a href="#!" class="b-b-primary text-primary">View all Task</a>
</div>


</div>
</div>
</div>
	</div>
</div>



@endsection

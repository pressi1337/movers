<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use JWTAuth;
use Validator;
use Crypt;


class AuthController extends Controller
{
	protected $user;

	public function __construct()
	{
	   $this->user = new User;
	}
    

    /**
     * Register User and Owner with differentiate
     *
     */
    public function register(Request $request){

    	$validator = Validator::make($request->all(),
	    [
	      'name'=>'required|string',
	      'mobile'=>'required|string',
	      'email'=>'required|email',
	      'istype'=>'required|string',
	      'password'=>'required|string|min:6',
	      'address'=>'required|string'
	    ]);

	    if($validator->fails())
	    {
	     return response()->json([
	         "success"=>false,
	         "message"=>$validator->messages()->toArray(),
	     ],400);
	    }

	    $check_mobile = $this->user->where("mobile",$request->mobile)->where("otp_verified","yes")->count();

	    if($check_mobile > 0)
	    {
	        return response()->json([
	        'success'=>false,
	        'message'=>'this Mobile number already exist'
	        ],400);

	    }

	    $rembertoken = Crypt::encrypt($request->password);

	    $registerComplete = $this->user::create([
        'name'=>$request->name,
        'mobile'=>$request->mobile,
        'email'=>$request->email,
        'istype'=>$request->istype,
        'password'=> Hash::make($request->password), 
        'otp'=>rand(1111,9999),
        'remember_token'=>$rembertoken,
        'address'=>$request->address,
        'block'=>'active'
        ]);

        //sms Api want to add


        if($registerComplete)
	    {
	       return response()->json([
	         "success"=>false,
	         "message"=>'Verify the OTP Sented to Your Mobile Number',
	         "rembertoken"=>$rembertoken
	       ],200);
	    }  

    }

    /**
     * Verify the OTP
     *
     */
    public function verify_otp(Request $request){

    	$validator = Validator::make($request->all(),
	    [
	      'otp'=>'required|integer',
	      'rembertoken'=>'required'
	    ]);

	    if($validator->fails())
	    {
	     return response()->json([
	         "success"=>false,
	         "message"=>$validator->messages()->toArray(),
	     ],400);
	    }

	    $check_vaild_otp = $this->user->where('otp',$request->otp)->where('remember_token',$request->rembertoken)->count();
	    if($check_vaild_otp){
            $this->user->where('remember_token',$request->rembertoken)->update(['otp_verified'=>'yes']);
            $attempt_to_login_get_data = $this->user->where('remember_token',$request->rembertoken)->where('otp_verified','yes')->first();
	    	
	    	// $request->mobile = $attempt_to_login_get_data->mobile;
	    	// $request->password = Crypt::decrypt($request->rembertoken);
	    	$request->merge([
			    'mobile' => $attempt_to_login_get_data->mobile,
			    'password' => Crypt::decrypt($request->rembertoken),
			]);

	    	return $this->login($request);

	    }else{
	    	return response()->json([
	        'success'=>false,
	        'message'=>'invalid otp number'
	        ],400);
	    }


    }


    /**
     * Verify Mobile Number to get OTP
     *
     */
    public function verifyMobileToForgotPassword(Request $request){
    	$validator = Validator::make($request->all(),
	    [
	      'mobile'=>'required',
	    ]);

	    if($validator->fails())
	    {
	     return response()->json([
	         "success"=>false,
	         "message"=>$validator->messages()->toArray(),
	     ],400);
	    }

        //check vaild mobile number
	    $check_mobile = $this->user->where("mobile",$request->mobile)->where("otp_verified","yes")->count();

	    if($check_mobile == 0)
	    {
	        return response()->json([
	        'success'=>false,
	        'message'=>'invalid mobile number'
	        ],400);

	    }

	    if($check_mobile){
	    	$generateOtp = rand(1111,9999);
	    	$rembertoken = Crypt::encrypt($generateOtp);
	    	$this->user->where('mobile',$request->mobile)->where("otp_verified","yes")->update(['otp'=>$generateOtp,
	    		'remember_token'=>$rembertoken,
	    ]);
	    	//sent otp to sms api


	    	return response()->json([
	        'success'=>false,
	        'message'=>'OTP is sented to your mobile number',
	        'rembertoken'=>$rembertoken
	        ],200);

	    }

    }

    /**
     * Verify OTP To update password
     *
     */
    public function verifyOtpToUpdatePassword(Request $request){

    	$validator = Validator::make($request->all(),
	    [
	      'otp'=>'required|integer',
	      'rembertoken'=>'required'
	    ]);

	    if($validator->fails())
	    {
	     return response()->json([
	         "success"=>false,
	         "message"=>$validator->messages()->toArray(),
	     ],400);
	    }

	    $check_vaild_otp = $this->user->where('otp',$request->otp)->where('remember_token',$request->rembertoken)->count();
	    if($check_vaild_otp){
            
            $rembertoken = Crypt::encrypt($request->otp);
	    	$this->user->where('remember_token',$request->rembertoken)->where('otp',$request->otp)->update(['remember_token'=>$rembertoken]);
	    	return response()->json([
	        'success'=>false,
	        'message'=>'otp verified successfully',
	        'rembertoken'=>$rembertoken
	        ],200);

	    }else{
	    	return response()->json([
	        'success'=>false,
	        'message'=>'invalid otp number'
	        ],400);
	    }

    }


    /**
     * Verify the password and update new password
     *
     */
    public function updateNewPassword(Request $request){
    	$validator = Validator::make($request->all(),
	    [
	      'rembertoken'=>'required',
	      'newpassword'=>'required|string|min:6',
	    ]);

	    if($validator->fails())
	    {
	     return response()->json([
	         "success"=>false,
	         "message"=>$validator->messages()->toArray(),
	     ],400);
	    }

	    //check vaild token
	    $check_vaild_token = $this->user->where("remember_token",$request->rembertoken)->where("otp_verified","yes")->count();

	    if($check_vaild_token == 0)
	    {
	        return response()->json([
	        'success'=>false,
	        'message'=>'invalid token'
	        ],400);

	    }

	    if($check_vaild_token){
	    	$NewPassword = Hash::make($request->newpassword);
	    	//update Newpassword
	    	$this->user->where('remember_token',$request->rembertoken)->where("otp_verified","yes")->update(['password'=>$NewPassword]);
	    	return response()->json([
	        'success'=>false,
	        'message'=>'New password update successfully'
	        ],200);
	    }

    }

    /**
     * Login
     *
     */
    public function login(Request $request){

    	$validator = Validator::make($request->only('mobile','password'),
	     [
	        'mobile'=>'required',
	        'password'=>'required|string',
	     ]);

    	 if($validator->fails())
	     {
		     return response()->json([
		         "success"=>false,
		         "message"=>$validator->messages()->toArray(),
		     ],400);
	     }

	     $jwt_token = null;

	     $input = $request->only("mobile","password");

	     $check_mobile = $this->user->where("mobile",$request->mobile)->where("otp_verified","yes")->count();

	    if($check_mobile == 0)
	    {
	        return response()->json([
	        'success'=>false,
	        'message'=>'invalid mobile number'
	        ],400);

	    }
	     //Authentication
	     if(!$jwt_token = auth('users')->attempt($input))
	     {
	        return response()->json([
	            'success'=>false,
	            'message'=>'invalid password'
	        ],400);

	      }
	      $user = auth("users")->authenticate($jwt_token);

	     return response()->json([
			  'success'=>true,
			  'istype'=>$user->istype,
			  'token'=>$jwt_token,
			 ],200);

    }



}

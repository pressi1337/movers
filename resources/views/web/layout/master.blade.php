
<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<title>Mover Web Portal</title>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<meta name="author" content="#">

<link rel="icon" href="{{ url('') }}/admin/files/assets/images/favicon.ico" type="image/x-icon">

<link href="https://fonts.googleapis.com/css3b0a.css?family=Open+Sans:400,600,800" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/bower_components/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/assets/icon/themify-icons/themify-icons.css">

<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/assets/icon/icofont/css/icofont.css">
<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/assets/icon/flag-icons/css/flag-icon.css">
<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/assets/icon/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/assets/icon/feather/css/feather.css">

<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/assets/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/assets/pages/data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ url('') }}/admin/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
</head>

<body>

<div class="theme-loader">
<div class="ball-scale">
<div class='contain'>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
<div class="ring">
<div class="frame"></div>
</div>
</div>
</div>
</div>

<!-- pcode start -->
<div id="pcoded" class="pcoded">

	<!-- header  -->
	@include('web.layout.header')
	<!-- ./header -->

	<!-- page conatiner -->
<div class="pcoded-container navbar-wrapper">
	<div class="pcoded-main-container">
          <div class="pcoded-wrapper">
          	<!-- slidebar -->
          	@include('web.layout.sidebar')
          	<!-- ./sidebar -->
          </div>
          <div class="pcoded-content">
             <div class="pcoded-inner-content">
             	<div class="main-body">
                   <div class="page-wrapper">
                        <div class="page-body">
		             	<!-- inner content-->
		             	@yield('content')
		             	<!-- ./inner content-->
		                </div>
		            </div>
		        </div>
             </div>
          </div>
    </div>
</div>

</div>

<!-- script -->
 @include('web.layout.script')
<!-- ./script -->
</body>
</html>

